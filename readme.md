Informações de acesso e configurações:



1 - Informações gerais;
2 - Banco de dados;
3 - Login;
4 - Acesso WebService

//**************************************************************//

1 - Informações gerais.

- O sistema todo, foi feito com o Framework CodeIgniter;

-Para rodar o sistema, é necessário configurar o arquivo "config.ini", que está na pasta "/teste_ead/sistema/application/config/config.php",
com os dados da url padrão, que no meu caso está "http://localhost/teste_ead/sistema".

2 - Banco de dados

- Para que o sistema funcione, também deve-se, configurar os acessos ao banco de dados, no arquivo "/teste_ead/sistema/application/config/database.php",

atualmente com as configurações:
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'teste_ead'

devendo ser mudadas, pelo executor do sistema;

Ps: o sql do banco de dados está na pasta raiz "teste_ead/sql_teste_ead.sql" e é necessário criar o banco de dados com o nome "teste_ead" no phpmyadmin

3 - Login

- O usuário padrão para login é:
	usuário: admin
	senha: admin

4 - Acesso Webservice

- Para acessar o webservice, é necessário configurar o arquivo "config.ini", que está na pasta "/teste_ead/ws/application/config/config.php",
com os dados da url padrão, que no meu caso está "http://localhost/teste_ead/ws".

- Para retornar os dados de todos os serviços, o endereço é: "http://<url_do_projeto>/teste_ead/ws/servicos/", e ele irá retornar
o id e nome de todos os serviços cadastrados no banco.