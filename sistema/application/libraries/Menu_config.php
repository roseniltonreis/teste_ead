<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| MODULOS
|--------------------------------------------------------------------------
|
|
|
*/

class Menu_config {

    public function init()
    {
    	return  array(
			'menuHome' =>array(
				'nome'=>'Home',
				'icone'=>'fa fa-home',
				'url'=>base_url()."home",
			),
			'menuAdmin'=>array(
				'nome'=>'Admin',
				'icone'=>'fa fa-lock',
				'submenu'=> array(
					array(
						'nome'=>'Logs',
						'url'=>base_url()."admin/log"
					),
					array(
						'nome'=>'Listar categorias',
						'url'=>base_url()."categoria"
					),
					array(
						'nome'=>'Nova categoria',
						'url'=>base_url()."categoria/novo"
					),
				)
			),
			'menuBoleto'=>array(
				'nome'=>'Boleto',
				'icone'=>'fa fa-barcode',
				'submenu'=> array(
					array(
						'nome'=>'Listar Boletos',
						'url'=>base_url()."boleto"
					),
					array(
						'nome'=>'Novo Boleto',
						'url'=>base_url()."boleto/novo"
					),
					array(
						'nome'=>'Configurar Boleto',
						'url'=>base_url()."boleto/config"
					)
				)
			),
			'menuCliente'=>array(
				'nome'=>'Cliente',
				'icone'=>'fa fa-user',
				'submenu'=> array(
					array(
						'nome'=>'Listar Clientes',
						'url'=>base_url()."cliente"
					),
					array(
						'nome'=>'Novo Cliente',
						'url'=>base_url()."cliente/novo"
					)
				)
			),
			'menuRelatorio' =>array(
				'nome'=>'Relatorio',
				'icone'=>'fa fa-file-word-o',
				'url'=>base_url()."",
			),
			'menuConfig' =>array(
				'nome'=>'Configurações',
				'icone'=>'fa fa-gear',
				'url'=>base_url()."configuracoes",
			),
			'menuSms' =>array(
				'nome'=>'SMS',
				'icone'=>'fa fa-envelope-square',
				'url'=>base_url()."",
			),
		);
    }
}

/* End of file Someclass.php */




