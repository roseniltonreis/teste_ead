   <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script><!-- jQuery -->
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.min.js"></script><!-- jQuery -->
    <!-- NOVO CLIENTE -->

            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>Nova Aquisição de serviço</h1>
                        <span class="st-border"></span>
                    </div>
                </div>


          <form id="form_cliente" class="form-horizontal form-label-left">
            <div class="form-group">

              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">Cliente</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select required class="form-control form-control col-md-7 col-xs-12" tabindex="-1" name="cliente_id" id="cliente_id">
                    <option>Selecione um cliente</option>
                    <?php
                      foreach ($clientes as $cliente) {
                        echo('<option value="'.$cliente->cliente_id.'">'.$cliente->nome.'</option>');
                      }
                    ?>
                      
                  </select>
                </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">Serviço</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select required class="form-control form-control col-md-7 col-xs-12" tabindex="-1" name="servico_id" id="servico_id">
                    <option>Selecione um Serviço</option>
                    <?php
                      foreach ($servicos as $servico) {
                        echo('<option value="'.$servico->servico_id.'">'.$servico->nome.'</option>');
                      }
                    ?>                      
                  </select>
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">Data de início</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input required class="form-control col-md-7 col-xs-12" placeholder="Digite a data de início" name="data_inicio" id="data_inicio" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">Data de término</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input required class="form-control col-md-7 col-xs-12" placeholder="Digite a data de término" name="data_fim" id="data_fim" type="text">
              </div>
            </div>  
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button type="submit" class="btn btn-success" id="btnSalvar">Salvar</button>
                <button type="button" onclick="window.location.href='<?php echo base_url();?>'" class="btn btn-primary">Cancelar</button>
              </div>
            </div>

          </form>

            </div>
    <!-- /NOVO CLIENTE -->


  <section id="contact">
    <div class="container">
     <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID do Serviço</th>
                            <th>Nome do Cliente</th>
                            <th>Serviço prestado</th>
                            <th>Data de início</th>
                            <th>Data de término</th>
                            <th>Dias restantes</th>
                        </tr>
                    </thead>
                       <tbody>
              <?php
              //variável $clientes, vem por post do controller "clientes.php"

              foreach ($vendas_servicos as $vendas_servico) {
                $linha = "<tr class='even pointer'>";
                // $linha .= "<td>".$vendas_servico->vendas_servico_id."</td>";
                $linha .= "<td>".$vendas_servico['servico_cliente_id']."</td>";
                $linha .= "<td>".$vendas_servico['nome_cliente']."</td>";
                $linha .= "<td>".$vendas_servico['nome_servico']."</td>";
                $linha .= "<td>".$vendas_servico['data_ini']."</td>";
                $linha .= "<td>".$vendas_servico['data_fim']."</td>";
                $linha .= "<td>".$vendas_servico['dias_restantes']."</td>";
              $linha .= "</tr>";
              //lista os resultados na tabela
              echo $linha;
            }
            ?>

          </tbody>
    </table>          
    </div>
  </section>


    <script>

      $('#data_inicio').mask('00/00/0000');
      $('#data_fim').mask('00/00/0000');

      $( "#form_cliente" ).submit(function( e ) {

      e.preventDefault();
      e.stopPropagation();

      $.ajax({
        url: "<?php echo base_url('venda_servico/salvar');?>",
        type: 'POST',
        data: $(this).serialize(),
      })
      .always(function(data) {
        if(data == "ok"){
          alert("Venda salva com sucesso");
          window.location="<?php echo base_url('venda_servico'); ?>";
        } else{
          alert("Erro ao salvar Cliente!");
        } //fim: else
      });

});
    </script>