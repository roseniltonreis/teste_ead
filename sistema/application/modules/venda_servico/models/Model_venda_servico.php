<?php
class Model_venda_servico extends CI_Model{

	/**
	 * Salva formulário de venda_servico
	 * @param array $dados dados a serem salvos no banco
	 */
	public function save_form($dados=NULL){
		if($dados != NULL):
			$query = $this->db->insert('servico_cliente', $dados);
			if($query) return true;
            // $this->session->set_flashdata('cadastrook', 'Cadastro efetuado com sucesso');
		endif;
	}


	/**
	 * Retorna todos os registros da tabela venda_servico
	 */
	public function list_venda_servicos()
	{
		return $this->db->get('servico_cliente');
	}

	public function venda_servico_resumo(){
		$query = $this->db->query('SELECT v.servico_cliente_id, c.nome as nomeCli, s.nome as nomeServ, v.data_inicio, v.data_fim, DATEDIFF(v.data_fim, v.data_inicio) as diferenca FROM servico_cliente v
			INNER JOIN cliente c ON c.cliente_id = v.cliente_id
			INNER JOIN servico s ON s.servico_id = v.servico_id'
		)->result_array();

		$result = array();

		foreach ($query as $q){


			$data_inicio = $q["data_inicio"];
			$data_fim = $q["data_fim"];

			$data_inicio = str_replace("-", "/", $data_inicio);
			$data_fim = str_replace("-", "/", $data_fim);

			//formata datas para salvar no banco
			$data_inicio = date("d/m/Y", strtotime($data_inicio));
			$data_fim = date("d/m/Y", strtotime($data_fim));



			if($q["diferenca"] >= 1){
				$dias_restantes = $q["diferenca"];
			}else{
				$dias_restantes = "serviço já vencido!";
			}
			array_push($result, array("servico_cliente_id"=>$q["servico_cliente_id"], "nome_cliente"=>$q['nomeCli'], "nome_servico"=>$q['nomeServ'], "data_ini"=>$data_inicio, "data_fim"=>$data_fim, "dias_restantes"=>$dias_restantes));
		}

		return $result;
	}
}