<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venda_servico extends MY_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata("logado")){
			redirect('login');
		}
		$this->load->helper('array');
		$this->load->model("Model_venda_servico", "venda_servico_model");
		$this->load->model("cliente/Model_cliente", "model_cliente");
		$this->load->model("servico/Model_servico", "model_servico");
		$this->load->model("venda_servico/Model_venda_servico", "model_venda_servico");
	}

	public function index()
	{
		$dados = array(
			'tela'=>'pages/novo',
			'clientes'=> $this->model_cliente->list_clientes()->result(),
			'servicos'=> $this->model_servico->list_servicos()->result(),
			'vendas_servicos'=>$this->model_venda_servico->venda_servico_resumo()
			);

		$this->load->view('venda_servico', $dados);
	}



	public function salvar(){

		$data_inicio = $this->input->post("data_inicio");
		$data_fim = $this->input->post("data_fim");

		$data_inicio = str_replace("/", "-", $data_inicio);
		$data_fim = str_replace("/", "-", $data_fim);

		//formata datas para salvar no banco
		$data_inicio = date("Y-m-d", strtotime($data_inicio));
		$data_fim = date("Y-m-d", strtotime($data_fim));


		$valoresPost = array(
			'servico_id'=> $this->input->post("servico_id"),
			'cliente_id'=> $this->input->post("cliente_id"),
			'data_inicio'=> $data_inicio,
			'data_fim'=> $data_fim
		);

		$dados_form = elements(array('servico_id','cliente_id','data_inicio','data_fim'), $valoresPost);


		$result =  $this->venda_servico_model->save_form($dados_form);

		if($result){
			echo("ok");
		} else{
			echo("erro");
		}
    } //fim: salvar

 }
