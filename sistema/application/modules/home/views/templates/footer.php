        </div>
    </section>
    <!-- FOOTER -->

    <!-- /FOOTER -->


    <!-- Scroll-up -->
    <div class="scroll-up">
        <ul><li><a href="#header"><i class="fa fa-angle-up"></i></a></li></ul>
    </div>

    
    <!-- JS -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script><!-- jQuery -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script><!-- Bootstrap -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.parallax.js');?>"></script><!-- Parallax -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/smoothscroll.js');?>"></script><!-- Smooth Scroll -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/masonry.pkgd.min.js');?>"></script><!-- masonry -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.fitvids.js');?>"></script><!-- fitvids -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/owl.carousel.min.js');?>"></script><!-- Owl-Carousel -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.counterup.min.js');?>"></script><!-- CounterUp -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/waypoints.min.js');?>"></script><!-- CounterUp -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.isotope.min.js');?>"></script><!-- isotope -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.magnific-popup.min.js');?>"></script><!-- magnific-popup -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/scripts.js');?>"></script><!-- Scripts -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/scripts.js');?>"></script><!-- Scripts -->
    <script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script><!-- Scripts -->


</body>
</html>