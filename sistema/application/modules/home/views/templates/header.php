<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">

    <title>Teste EAD</title>

    <!-- Main CSS file -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/magnific-popup.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css');?>" />


    
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/icon/favicon.png');?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/images/icon/apple-touch-icon-144-precomposed.png');?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/images/icon/apple-touch-icon-114-precomposed.png');?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/images/icon/apple-touch-icon-72-precomposed.png');?>">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/images/icon/apple-touch-icon-57-precomposed.png');?>">
    <link rel="apple-touch-icon-precomposed" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>
<body>

    <!-- PRELOADER -->
    <div id="st-preloader">
        <div id="pre-status">
            <div class="preload-placeholder"></div>
        </div>
    </div>
    <!-- /PRELOADER -->

    
    <!-- HEADER -->
    <header id="header">
        <nav class="navbar st-navbar navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#st-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a style="font-size:30px; line-height: 1; font-weight: bold;"href="index.html">Rosenilton Reis</a>
                </div>

                <div class="collapse navbar-collapse" id="st-navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><?php echo anchor('cliente', 'Clientes cadastrados'); ?></li>
                        <li><?php echo anchor('cliente/novo', 'Novo Cliente'); ?></li>
                        <li><?php echo anchor('servico', 'Serviços'); ?></li>
                        <li><?php echo anchor('venda_servico', 'Venda'); ?></li>
                        <li><?php echo anchor('login/logout', 'Logout'); ?></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
        </nav>
    </header>
    <!-- /HEADER -->
        <!-- CONTACT -->
    <section id="contact">
    <div class="container">