<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Home
 * 
 * @package modules/home/controllers/
 * @author Rosenilton Reis
 * 
 */
class Home extends MY_Controller {
    function __construct(){
        parent::__construct();
        /**
       * Verifica se usuário está logado
       * se não estiver redirecioa para
       * a página de login
       */
        if(!$this->session->userdata("logado")){
            redirect('login');
        }
    }

   /**
   * Primeiro método chamado depois do construtor 
   */
    public function index()
    {
        $dados = array(
            'title'=>'Home',
            'breadcrumb'=>'',
            'icone'=>'',
            'tela'=>'',
        );
        $this->load->view('home', $dados);
    }

    /**
     * Carrega a tela de login
     */
    public function login(){
          $dados = array(
            'title'=>'Home',
            'tela'=>'login',
        );
        $this->load->view("home", $dados);
    }
}
