<?php
class Model_login extends CI_Model{
    public function buscaPorEmailSenha($login, $senha){
        $this->db->where("login", $login);
        $this->db->where("senha", $senha);
        $usuario = $this->db->get("usuarios")->row_array();

        if($usuario > 0){
            $dadosUsuario = array(
                'user'  => $usuario['nome'],
                'login'     => $usuario['login'],
                'user_id' => $usuario['usuario_id'],
                'permissao' => $usuario['permissao'],
                'logado' => TRUE
            );
            $this->session->set_userdata($dadosUsuario);
        }

        //log de acesso

        return $usuario;
    }
}