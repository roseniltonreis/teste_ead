<?php
class Model_cliente extends CI_Model{

	/**
	 * Salva formulário de cliente
	 * @param array $dados dados a serem salvos no banco
	 */
	public function save_form($dados=NULL){
		if($dados != NULL):
			$query = $this->db->insert('cliente', $dados);
			if($query) return true;
		endif;
	}

	public function update_form($dados=NULL, $condicao){
		if($dados != NULL):
			$query = $this->db->where("cliente_id", $condicao)->update('cliente', $dados);
			if($query) return true;
		endif;
	}

	/**
	 * Retorna todos os registros da tabela cliente
	 */
	public function list_clientes()
	{
		return $this->db->get('cliente');
	}

	public function delete_reg($id=NULL)
	{
		$query = $this->db->delete('cliente', array('cliente_id' => $id)); 
		
		return $this->db->affected_rows();
	}

	public function get_cliente_by_id($id='')
	{		
		$query =  $this->db->get_where('cliente', array("cliente_id"=>$id));
		return $query->row();
	}

}