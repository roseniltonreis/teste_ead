<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends MY_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata("logado")){
			redirect('login');
		}
		$this->load->helper('array');
		$this->load->model("Model_cliente", "cliente_model");
	}

	public function index()
	{
		$dados = array(
			'tela'=>'pages/listar',
      		'clientes'=>$this->cliente_model->list_clientes()->result()
			);
		$this->load->view('cliente', $dados);
	}

	public function novo(){
		$dados = array(
			'tela'=>'pages/novo',
			);
		$this->load->view('cliente', $dados);
	}

	public function update(){
		$dados = array(
			'tela'=>'pages/alterar',
			'cliente'=> $this->cliente_model->get_cliente_by_id($this->uri->segment(3))
			);
		$this->load->view('cliente', $dados);
	}

	public function salvar(){

		$valoresPost = array(
			'nome'=> $this->input->post("nome"),
			'rg'=> $this->input->post("rg"),
			'data_cadastro'=> date("Y-m-d"),
			
			);

		$dados_form = elements(array('nome', 'rg', 'data_cadastro'), $valoresPost);


		$result =  $this->cliente_model->save_form($dados_form);

		if($result){
			echo("ok");
		} else{
			echo("erro");
		}
    } //fim: salvar

	public function alterar(){

		$id = $this->input->post("id");

		$valoresPost = array(
			'nome'=> $this->input->post("nome"),
			'rg'=> $this->input->post("rg"),
			
			);

		$dados_form = elements(array('nome', 'rg'), $valoresPost);
		$result =  $this->cliente_model->update_form($dados_form, $id);

		if($result){
			echo("ok");
		} else{
			echo("erro");
		}
    } //fim: alterar

    public function deletar(){
    	$id = $this->input->get("id");

    	$result = $this->cliente_model->delete_reg($id);

    	if($result){
    		echo("ok");
    	} else{
    		echo("erro");
    	}
    }
 }
