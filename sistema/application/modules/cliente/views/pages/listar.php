   
    <!-- CONTACT -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>Clientes</h1>
                        <span class="st-border"></span>
                    </div>
                </div>


                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>RG</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                       <tbody>
							<?php
							//variável $clientes, vem por post do controller "clientes.php"

							foreach ($clientes as $cliente) {

								$data = $cliente->data_cadastro;
								$data = str_replace("-", "/", $data);
								$data = date("d/m/Y", strtotime($data));

								$linha = "<tr class='even pointer'>";
								// $linha .= "<td>".$cliente->cliente_id."</td>";
								$linha .= "<td>".$cliente->cliente_id."</td>";
								$linha .= "<td>".$cliente->nome."</td>";
								$linha .= "<td>".$cliente->rg."</td>";
								$linha .= "<td>".$data."</td>";
								$linha .= '<td class="center">
								<a class="btn btn-info" href="'.base_url().'cliente/update/'.$cliente->cliente_id.'">
									<i class="fa fa-edit"></i>
								</a>
								<a class="btn btn-warning" onclick="deletar('.$cliente->cliente_id.')">
									<i class="fa fa-trash"></i>
								</a>
							</td>';
							$linha .= "</tr>";
							//lista os resultados na tabela
							echo $linha;
						}
						?>

					</tbody>
                </table>          
            </div>
        </div>
    </section>
    <!-- /CONTACT -->


    <script>
	    function deletar(id){
			$.ajax({
		        url: "<?php echo base_url('cliente/deletar');?>"+"?id="+id,
		        type: 'get'
		      })
		      .always(function(data) {
	           	if(data == "ok"){
	        		alert("Registro deletado com sucesso!");
         			window.location="<?php echo base_url('cliente'); ?>";

	        	}
		    });
	    }
    </script>