   <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script><!-- jQuery -->
    <!-- ALTERAÇÃO DE CLIENTE -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>Novo Cliente</h1>
                        <span class="st-border"></span>
                    </div>
                </div>
                <?php
                  $id = $this->uri->segment(3);
                  if(!$id){
                    redirect('cliente', 'refresh');
                  }
                ?>

          <form id="form_cliente" class="form-horizontal form-label-left">
            <input type="hidden" value="<?php echo $cliente->cliente_id; ?>" name="id">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">Nome</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input required class="form-control col-md-7 col-xs-12" placeholder="Digite o nome do cliente" name="nome" id="nome" type="text" value="<?php echo $cliente->nome; ?>">
              </div>
            </div>
           <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rg">RG</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input required class="form-control col-md-7 col-xs-12" placeholder="Digite o telefone do cliente" name="rg" id="rg" type="text" value="<?php echo $cliente->rg; ?>">
              </div>
            </div>
  
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button type="submit" class="btn btn-success" id="btnSalvar">Salvar</button>
                <button type="button" onclick="window.location.href='<?php echo base_url('cliente');?>'" class="btn btn-primary">Cancelar</button>
              </div>
            </div>

          </form>

            </div>
        </div>
    </section>
    <!-- /ALTERAÇÃO DE CLIENTE -->


    <script>
      $( "#form_cliente" ).submit(function( e ) {

   // $("#btnSalvar").attr('disabled', 'disabled');
      e.preventDefault();
      e.stopPropagation();

      $.ajax({
        url: "<?php echo base_url('cliente/alterar');?>",
        type: 'POST',
        data: $(this).serialize(),
      })
      .always(function(data) {
        if(data == "ok"){
          alert("Cliente salvo com sucesso");
          window.location="<?php echo base_url('cliente'); ?>";
        } else{
          alert("Erro ao alterar Cliente!");
        } //fim: else
      });

});
    </script>