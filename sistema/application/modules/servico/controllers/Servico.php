<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servico extends MY_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata("logado")){
			redirect('login');
		}
		$this->load->helper('array');
		$this->load->model("Model_servico", "servico_model");
	}

	public function index()
	{
		$dados = array(
			'title'=>'Home',
			'breadcrumb'=>'Lista servicos',
			'icone'=>'icon-file-alt',
			'tela'=>'pages/novo',
      		'servicos'=>$this->servico_model->list_servicos()->result()
			);
		$this->load->view('servico', $dados);
	}

	public function novo(){
		$dados = array(
			'title'=>'Novo servico',
			'breadcrumb'=>'Novo servico',
			'icone'=>'icon-user',
			'tela'=>'pages/novo',
			);
		$this->load->view('servico', $dados);
	}

	public function update(){
		$dados = array(
			'tela'=>'pages/alterar',
			'servico'=> $this->servico_model->get_servico_by_id($this->uri->segment(3))
			);
		$this->load->view('servico', $dados);
	}

	public function salvar(){

		$valoresPost = array(
			'nome'=> $this->input->post("nome"),
			
			);

		$dados_form = elements(array('nome'), $valoresPost);


		$result =  $this->servico_model->save_form($dados_form);

		if($result){
			echo("ok");
		} else{
			echo("erro");
		}
    } //fim: salvar

	public function alterar(){

		$id = $this->input->post("id");

		$valoresPost = array(
			'nome'=> $this->input->post("nome"),
			'rg'=> $this->input->post("rg"),
			
			);

		$dados_form = elements(array('nome', 'rg'), $valoresPost);
		$result =  $this->servico_model->update_form($dados_form, $id);

		if($result){
			echo("ok");
		} else{
			echo("erro");
		}
    } //fim: alterar

    public function deletar(){
    	$id = $this->input->get("id");

    	$result = $this->servico_model->delete_reg($id);

    	if($result){
    		echo("ok");
    	} else{
    		echo("erro");
    	}
    }



 }
