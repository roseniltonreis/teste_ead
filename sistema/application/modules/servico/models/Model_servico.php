<?php
class Model_servico extends CI_Model{

	/**
	 * Salva formulário de servico
	 * @param array $dados dados a serem salvos no banco
	 */
	public function save_form($dados=NULL){
		if($dados != NULL):
			$query = $this->db->insert('servico', $dados);
			if($query) return true;
            // $this->session->set_flashdata('cadastrook', 'Cadastro efetuado com sucesso');
		endif;
	}

	public function update_form($dados=NULL, $condicao){
		if($dados != NULL):
			$query = $this->db->where("servico_id", $condicao)->update('servico', $dados);
			if($query) return true;
            // $this->session->set_flashdata('cadastrook', 'Cadastro efetuado com sucesso');
		endif;
	}

	/**
	 * Retorna todos os registros da tabela servico
	 */
	public function list_servicos()
	{
		return $this->db->get('servico');
	}

	public function delete_reg($id=NULL)
	{
		$query = $this->db->delete('servico', array('servico_id' => $id)); 
		
		return $this->db->affected_rows();
	}

	public function get_servico_by_id($id='')
	{		
		$query =  $this->db->get_where('servico', array("servico_id"=>$id));
		return $query->row();
	}

	public function get_byid($q='')
	{
		$this->db->select('*');
		$this->db->like('nome', $q);
		$query = $this->db->get('servico');

		if($query->num_rows() > 0){
			foreach ($query->result_array() as $row){
				$new_row['label']=htmlentities(stripslashes($row['nome']));
				$new_row['id']=htmlentities(stripslashes($row['servico_id']));
		        $row_set[] = $new_row; //build an array
		     }
		      echo json_encode($row_set); //format the array into json data
		   }

		// * Example JSON format
		// {
		//   "item1": "I love jquery4u",
		//   "item2": "You love jQuery4u",
		//   "item3": "We love jQuery4u"
		// }
		// */

		// //return in JSON format
		// echo "{";
		// echo "item1: ", json_encode($item1), "n";
		// echo "item2: ", json_encode($item2), "n";
		// echo "item3: ", json_encode($item3), "n";
		// echo "}";




		// $this->db->like("nome", $search);
		// $query =  $this->db->get('servico');
		// $json = "";
		// foreach ($query->result_array() as $row)
		// {

		// 	$json .="{";
		// 	$json .= "nome: ".json_encode($row["nome"]);
		// 	$json .="}";
		// }
		// return json_encode($json);
	}

}