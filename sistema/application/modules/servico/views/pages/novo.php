   <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script><!-- jQuery -->
    <!-- NOVO CLIENTE -->

            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>Novo Serviço</h1>
                        <span class="st-border"></span>
                    </div>
                </div>


          <form id="form_cliente" class="form-horizontal form-label-left">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nome">Serviço</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input required class="form-control col-md-7 col-xs-12" placeholder="Digite o nome do serviço" name="nome" id="nome" type="text">
              </div>
            </div>
  
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button type="submit" class="btn btn-success" id="btnSalvar">Salvar</button>
                <button type="button" onclick="window.location.href='<?php echo base_url();?>'" class="btn btn-primary">Cancelar</button>
              </div>
            </div>

          </form>

            </div>

    <!-- /NOVO CLIENTE -->


    <script>
      $( "#form_cliente" ).submit(function( e ) {

      e.preventDefault();
      e.stopPropagation();

      $.ajax({
        url: "<?php echo base_url('servico/salvar');?>",
        type: 'POST',
        data: $(this).serialize(),
      })
      .always(function(data) {
        if(data == "ok"){
          alert("Serviço salvo com sucesso");
          window.location="<?php echo base_url('servico'); ?>";
        } else{
          alert("Erro ao salvar Serviço!");
        } //fim: else
      });

});
    </script>