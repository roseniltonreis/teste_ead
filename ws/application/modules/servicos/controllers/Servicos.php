<?php
/**
 * Preflights Events
 * @author Thiago bardez
 * Tratamento do envio via OPTIONS;
 * Controle de CrossDomain;
 * tratamento para retorno e recepção de arquivos
 **/
 /*Início preflight events*/
header('Access-Control-Allow-Origin: *');
header( "Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS" );
header('Content-Type: application/json');

if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    header("Access-Control-Allow-Headers: content-type, origin, accept, x-app-sig");
    header( 'Access-Control-Allow-Origin: *' );
    header( "Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS" );
    header( 'Access-Control-Allow-Headers: content-type' );
    header( 'Access-Control-Request-Method: POST' );
    header( 'Access-Control-Request-Headers: X-PINGOTHER' );

    $acrh = explode(',', strtolower($headers['Access-Control-Request-Headers']));
    foreach ($acrh as $k => $v) {
        $acrh[$k] = trim($v);
    }

    if (! isset($headers['Access-Control-Request-Headers']) || ! in_array('x-app-sig', $acrh)) {
        _log($h, '*** Bad preflight!' . PHP_EOL . print_r($headers, true) . PHP_EOL . print_r($_REQUEST, true));
        header("HTTP/1.1 401 Unauthorized");
        exit; //->
    }

    _log($h, '+++ Successful preflight.' . PHP_EOL . print_r($headers, true) . PHP_EOL . print_r($_REQUEST, true));
    exit; //->
}
/*Fim preflight events*/

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class Home
 * 
 * @package modules/servicos/controllers/
 * 
 */
class Servicos extends REST_Controller {

	function __construct(){
		parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['servico_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['servico_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['servico_delete']['limit'] = 50; // 50 requests per hour per user/key

        $this->load->model("Model_servicos");
        $this->load->helper("array");
	}

	public function servico_get()
	{

        $servicos = $this->Model_servicos->all_servicos(); 


        if($servicos){
            $this->response($servicos, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Produto não cadastrado!'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
	}

	public function servico_post(){

	}

	public function servico_delete($id=NULL){

	}

	public function servico_put(){

	}
}
