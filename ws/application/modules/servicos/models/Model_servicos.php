<?php
class Model_servicos extends CI_Model{

	function __construct()
    {
        parent::__construct();
    }

    /**
     * @author Rosenilton Reis
     * @method Retorna todos as lojas da tabela servico com parametros id e nome do serviço
     */
	public function all_servicos(){
       $query = $this->db->get("servico");
        return $query->result_array();
	}

}